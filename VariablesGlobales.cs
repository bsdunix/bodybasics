﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Microsoft.Samples.Kinect.BodyBasics
{
    class VariablesGlobales
    {
        private static bool Alerta = false; 
        private static float v_HandRightX;
        private static float v_HandRightY;
        private static float v_HandRightZ;
        private static float v_HandLeftX;
        private static float v_HandLeftY;
        private static float v_HandLeftZ;
        
        //HandRightX , dentro se almacena v_HandRigthY
        public static float HandRightX
        {
            get { return v_HandRightX; }
            set
            {
                if (HandRightX >= 0.2)
                {
                    Alerta = true;
                }
                v_HandRightX = value;
            }
            //Realizar un ensayo con un array en el que guardo por ejemplo los 25 joints en un cajon de ese array,
            // mirar eso en internet y mirar como va el tema de coordenadas
        }
        
        public static float HandRightY
        {
            get { return v_HandRightY; }
            set { v_HandRightY = value; }
        }
        public static float HandRightZ
        {
            get { return v_HandRightZ; }
            set { v_HandRightZ = value; }
        }
        public static float HandLeftX
        {
            get { return v_HandLeftX; }
            set { v_HandLeftX = value; }
        }
        public static float HandLeftY
        {
            get { return v_HandLeftY; }
            set { v_HandLeftY = value; }
        }
        public static float HandLeftZ
        {
            get { return v_HandLeftZ; }
            set { v_HandLeftZ = value; }
        }
    }
}
