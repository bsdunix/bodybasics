﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Kinect;
using Microsoft.Samples.Kinect.BodyBasics;

// Noi putem sa schimbam namespace-ul. Namespace de obicei e ca adresa, ca denumirele in obiectul tau sa nu se incurce cu altele inafara.
namespace BodyBasics.BasicsBody
{
    public class MyBody
    {
        public static int TestMethod(string[] args)
        {
            VariablesGlobales.HandLeftX = 12.5f;
            VariablesGlobales.HandRightX = 6.129f;
            float[] ASampleArray = { VariablesGlobales.HandLeftX, VariablesGlobales.HandLeftY, VariablesGlobales.HandLeftZ, VariablesGlobales.HandRightX };
            // Folosim proprietatsile obiectului Body fara sa copiem tot contsinutul. Poate asta itsi trebue?
            int BodyClassJointCount = Body.JointCount;
            for (int item = 0; item < ASampleArray.Length; item++)
            {
                Console.WriteLine(ASampleArray);
            }
            Console.WriteLine("Hello World");
            Console.ReadLine();
            return 0;
        }
    }
}
